<?php

namespace Drupal\madness;

use Symfony\Component\EventDispatcher\Event;

/**
 * {@inheritdoc}
 */
class MadnessEvent extends Event {

  /**
   * The value of the message for the event to log.
   *
   * @var string
   */
  protected $message;

  /**
   * Implements __construct().
   *
   * @param string $message
   *   Log a message to indicate any madness changes.
   */
  public function __construct($message) {
    $this->message = $message;
  }

  /**
   * Gets messages for the event to log.
   *
   * @return string
   *   Message set by constructor.
   */
  public function getMessage() {
    return $this->message;
  }

}
